package apache_chain;

import org.apache.commons.chain.impl.CatalogBase;

/**
 * Created by kevingamboa17 on 10/6/17.
 */
public class AtmCatalog extends CatalogBase {

    public AtmCatalog() {
        super();
        addCommand("atmWithdrawalChain", new AtmWithdrawalChain());
    }
}

