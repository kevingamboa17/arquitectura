package kwic_chain;

import kwic_chain.commands.CombinationsGenerator;
import kwic_chain.commands.NumberOfPhrasesScanner;
import kwic_chain.commands.PhrasesScanner;
import org.apache.commons.chain.impl.ChainBase;

/**
 * Created by kevingamboa17 on 10/11/17.
 */
public class KwicGeneratorChain  extends ChainBase {
    public KwicGeneratorChain() {
        super();
        addCommand(new NumberOfPhrasesScanner());
        addCommand(new PhrasesScanner());
        addCommand(new CombinationsGenerator());
        addCommand(new KwicPrinterFilter());
    }
}
