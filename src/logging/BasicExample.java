package logging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by kevingamboa17 on 10/15/17.
 */
public class BasicExample {
    private static Log log = LogFactory.getLog(BasicExample.class);

    public static void main(String[] args) {
        log.debug("Example debug message ..");
        log.info("Example info message ..");
        log.warn("Example warn message ..");
        log.error("Example error message ..");
        log.fatal("Example fatal message ..");
    }
}
