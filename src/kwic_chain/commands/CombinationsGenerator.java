package kwic_chain.commands;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

import java.util.ArrayList;

/**
 * Created by kevingamboa17 on 10/11/17.
 */
public class CombinationsGenerator implements Command {
    @Override
    public boolean execute(Context context) throws Exception {
        int numberOfPhrases = (int)context.get("numberOfPhrases");
        String[] originalPhrases = (String[]) context.get("originalPhrases");
        ArrayList<String[]> combinations = new ArrayList<>();

        for (int i=0; i < numberOfPhrases; i++) {
            combinations.add(
                    generateCombinations(originalPhrases[i])
            );
        }

        context.put("phrasesCombinations", combinations);
        return false;
    }

    private String[] generateCombinations(String string) {
        String[] wordsOfString = string.split(" ");
        String[] lines = new String[wordsOfString.length];
        for (int x=0;x<wordsOfString.length;x++){
            lines[x] = getNewIndexes(wordsOfString, x);
        }
        return lines;
    }

    private String getNewIndexes(String[] words, int index){
        String word = "";
        for(int i=0;i<words.length;i++){
            if(i>=index){
                word += words[i] + " ";
            }
        }
        for(int j=0;j<words.length;j++){
            if(j<index){
                word += words[j] + " ";
            }
        }
        return word;
    }
}
