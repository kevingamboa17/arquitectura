package apache_chain.Commands;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

/**
 * Created by kevingamboa17 on 10/6/17.
 */
public class TenDenominationDispenser implements Command {

    @Override
    public boolean execute(Context context) throws Exception {
        int amountLeftToBeWithdrawn = (int) context.get("amountLeftToBeWithdrawn");
        if (amountLeftToBeWithdrawn >= 10) {
            context.put("noOfTensDispensed", amountLeftToBeWithdrawn / 10);
            context.put("amountLeftToBeWithdrawn", amountLeftToBeWithdrawn % 10);
        }
        return false;
    }
}
